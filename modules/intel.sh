#!/bin/sh

. ~/modules/module-exists.sh

module purge

module load intel/21.7.1
module load impi/2021.7

. ~/modules/common.sh

# if running interactively: set MPI variables; else stop
# see https://www.mpcdf.mpg.de/services/computing/linux/Astrophysics
case $- in
	*i*)
		unset I_MPI_HYDRA_BOOTSTRAP
		unset I_MPI_PMI_LIBRARY
		# if there are problems with multiple users using mpiexec at the same time
#		export I_MPI_FABRICS=shm
		;;
esac
