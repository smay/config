#!/bin/sh

. ~/modules/module-exists.sh

module load fftw-serial
module load hdf5-serial
module load gsl
module load hwloc
module load pkg-config
module load clang
module load datashare
module load git
module load parallel
module load anaconda/3/2023.03
module load mpi4py
module load julia/1.9
julia --startup-file=no -e \
	'using MPIPreferences; MPIPreferences.use_system_binary(library_names="'"$OPENMPI_HOME/lib/libmpi.so"'")' \
	>/dev/null 2>&1
module load gnuplot
module load p7zip
