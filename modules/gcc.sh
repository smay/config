#!/bin/sh

module purge

module load gcc/11
module load openmpi/4

. ~/modules/common.sh
