#!/bin/sh

module_exists() {
    module avail "$1" 2>&1 | grep -q "$1"
}
