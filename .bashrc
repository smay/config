# important environment variables must be set here since this file is loaded
# in /etc/profile, i.e. *before* ~/.profile!
. ~/.env

# setup modules
if [ -n "${BASHRC_LOAD_MODULES_INTEL+x}" ]; then
	. ~/modules/intel.sh
else
	. ~/modules/gcc.sh
fi
#module load mathematica  # only available on login nodes
#module load idl          # only available on login nodes


# stop if not running interactively
case $- in
	*i*)
		;;
	*) return;;
esac


# Don’t put duplicate lines or lines starting with space in the history.
# See bash(1) for more options.
HISTCONTROL=ignoreboth
# append to the history file, don’t overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=200000
HISTFILESIZE=320000
HISTTIMEFORMAT='%F %T '

# powerline (prompt)
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. ~/.local/lib/python3.10/site-packages/powerline/bindings/bash/powerline.sh

# additional alias definitions
if [ -f ~/.alias ]; then
        . ~/.alias
fi
