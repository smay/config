# Note that this file is loaded *after* ~/.bashrc, which is loaded in
# /etc/profile.
# Load /etc/profile here if it hasn’t been loaded yet.
test -z "$PROFILEREAD" && . /etc/profile || true


# stop if not running interactively
case $- in
    *i*)
        ;;
    *) return;;
esac


screen -dR
